FROM centos

LABEL project="Demo-Image"
LABEL maintainer "Karl"

RUN yum -y install httpd

EXPOSE 80

VOLUME /var/www/html

COPY data/ /var/www/html/

RUN echo "httpd" >> /root/.bashrc

CMD ["/bin/bash"]
